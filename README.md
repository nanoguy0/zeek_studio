# Zeek Studio

## v0.1.0

> `Based off of aeppert's visual studio extension `

[Aeppert's Github](https://github.com/aeppert/bro-vsc-extension)

Visual Studio Extension for Zeek/Bro Projects





# New features
*   `.bif` file highlighting
*   `option` is now included as variable type

# Getting Started
## Using Source
1. Download the source from [here](https://gitlab.com/nanoguy0/zeek_studio/-/releases)
2. Go to [homedir]/.vscode/extensions
3. Uncompress source folder into this directory


## Using VSIX Package
1. Download the latest release from [here](https://gitlab.com/nanoguy0/zeek_studio/-/releases)
2. Open VS Code and type `crt+shift+p` or `cmd+shift+p` based on if you are using windows/linux or mac.
3. Search for Install From VSIX
4. Select the VSIX Package


## Using VS Code Extension Marketplace
`Not Out Yet`

# Todo
*   Add Zeek compile functions to cmake
*   Add variable refrence in bif files
*   Add .sig Language highlighting